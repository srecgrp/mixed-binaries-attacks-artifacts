use std::os::raw::c_char;

#[no_mangle]
pub extern fn foo_rust(bug:fn(&[c_char;20])) {

   println!("-Calling bug using a Rust buffer\n");

   let buffer:[c_char;20] = [0;20];
   bug(&buffer);
}

