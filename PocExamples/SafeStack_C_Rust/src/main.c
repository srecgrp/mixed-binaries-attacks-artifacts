#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Input : r $(python -c 'print "A" * 20 + "\x30\xbe\x44"')


extern void foo_rust();

char *input = NULL;


void bug(char *buffer){

	strcpy(buffer, input);
	printf("-This is bug(C)\n");
}

void not_called(){
	printf("\n-This is not_called (C)\n");
}


int main(int argc, char *argv[]) {
   input = argv[1];
   if(argc>1){
      foo_rust(&bug);
   }
}




