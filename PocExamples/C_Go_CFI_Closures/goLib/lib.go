package main

/*#cgo CFLAGS: -g -O0
#include "cLib.h" */ 
import "C" 
    
import (
   "fmt"
)

type RetVal func(int) int

func sum(fpBug C.void_arg_fn,x int) RetVal{
   ret := func(y int) int {
      fmt.Println("-This is sum  - Inner return(Go) - Called because address overwrite failed\n")  
      return x + y 
   }
   C.bridge(fpBug);           //"Bug"
   return ret
}

//export go_func
func go_func (fpBug C.void_arg_fn) {
	fmt.Println(sum(fpBug,5)(6))
}



func main(){}

