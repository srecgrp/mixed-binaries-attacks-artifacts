#[no_mangle]
pub extern fn foo_rust(f:fn(i32)) {
   let a:i32 = 0 ; 

   println!("-This is foo_rust(Rust)\n");
	unsafe{
   	bug(&f);
	}
   f(a);
}
extern {
    fn bug(f:&fn(i32));
}

