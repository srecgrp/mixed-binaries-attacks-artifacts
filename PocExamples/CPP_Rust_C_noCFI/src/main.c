#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

extern void foo_rust(void);
extern void foo_cpp(void);


int option = 0;

void foo_C(){
   printf("-This is foo_C(C)\n\n");
}


void bug (long targetAddr){
	if(option == 0){
		printf("Rust bug\n");
		targetAddr += 0x8;

		long *ptr1 = (long *)targetAddr;
		long *ptr2 = (long *)(targetAddr+0x30);
		long fooAddress =(long)&foo_C;

		*ptr1 = (long) (targetAddr+0x18);
		*ptr2 =  fooAddress;
	}

	else if (option == 1){
		printf("C bug\n");

		long *ptr1 = (long *)targetAddr;
		targetAddr += 0x8;
		long *ptr2 = (long *)targetAddr;
		long fooAddress =(long)&foo_C;

		*ptr1 = (long) targetAddr;
		*ptr2 = fooAddress;
	}
}


int main(int argc, char* argv[]){

   if(argc != 1){
		option = argv[1][0] - '0';
		if(option == 0)
			foo_rust();
		else if (option == 1)
			foo_cpp();
   }  
   return 0;
}


