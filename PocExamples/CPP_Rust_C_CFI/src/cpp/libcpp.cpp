#include<iostream>
#include <list>
#include <vector>
#include "header.h"

using namespace std;

extern "C" void foo_cpp(void);

class Animal{
 public:
  
  virtual std::string make_sound()
  {
	return "c";
  }
};

class Cat: public Animal
{
 public:
  std::string make_sound()
  {
	return "meowc";
  }
};

class Dog:public Animal
{
 public:
	std::string make_sound()
  {
	return "woofc";
  }
};


void foo_cpp(void){

	vector<Animal*> v(2);
	Animal *v1 = new Cat();
	Animal *v2 = new Dog();

	v[0]=(v1);
	v[1]=(v2);

	Animal *p = v[1];
	long x = (long)p;
	bug(x);

	printf("CPP:\n");	

	for(std::vector<Animal*>::iterator it = v.begin(); it != v.end(); ++it) {
		printf("%s\n",(*it)->make_sound().c_str());
	}
}

