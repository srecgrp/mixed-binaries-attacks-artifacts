#include "goLib/cLib.h"
extern void go_func();



void foo_int(int a){
   printf("-This is foo_int(C)\n\n");
  
}
void foo_float(float a){
   printf("-This is foo_float(C) - Called because address overwrite succeeded\n\n");
  
}


void bug_for_c (void (**fpC) (int)){
   printf("-This is bug_for_c(C)\n\n");
   *fpC = (void(*)(int))&foo_float;

}

void bug_for_go (){
   printf("-This is bug_for_go(C)\n\n");
// x000000000049cfcd <+173>:	mov    0x30(%rsp),%rdx | 0xc420046e18 is the rsp value in go_func
   long targetAddr = 0xc420046e18 + 0x40;
   long *ptr = (long *)targetAddr;
   *ptr = (long)&foo_float;
   
   long helperAddr = targetAddr - 0x10;
   long *helperPtr = (long *)helperAddr;
   *helperPtr = targetAddr;

}

int main(int argc, char* argv[]){

   printf("-This is main(C)\n\n");


   void_arg_fn fpBug;
   fpBug = bug_for_go;         // Function pointer to bug_for_go


   go_func(fpBug);             //Call a Go function


                              //Demonstration of C-CFI
   void (*fpC) (int);
   fpC = &foo_int;
   bug_for_c(&fpC);
   fpC(0);


   return 0;
}



