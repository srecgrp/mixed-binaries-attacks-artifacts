package main

/*#include "cLib.h" */ 
import "C" 
    
import (
   "fmt"
)


func bar_go_int(a int)  {
   fmt.Println("-This is bar_go_int(Go) - Called because address overwrite failed\n")
}

func bar_go_float(a float32)  {
   fmt.Println("-This is bar_go_float(Go)\n")
}


//export go_func
func go_func (fpBug C.void_arg_fn) {
   fmt.Println("-This is go_func(Go)\n")

   var fpGo func(int)
   fpGo = bar_go_int
   C.bridge(fpBug);           //"Bug"

   fpGo(0)                    //Indirect call
   
}

func main(){}


