package main

/*#cgo CFLAGS: -g -O0
#include "cLib.h" */ 
import "C" 


import (
   "fmt"
   "time"
)
 
type chanType func() int
 

func foo_go(ch <-chan chanType,fpBug C.void_arg_fn) {
   fmt.Println("-This is foo_go(Go)\n")
   e := <-ch
   C.bridge(fpBug);           //"Bug"
   e()
}



func bar_go(ch chan<- chanType) {
   fmt.Println("-This is bar_go(Go)\n")
   ch <- func() int {
      fmt.Println("-This is bar_go - anonymous function(Go) - Called because address overwrite failed\n")
      return 0
   }
   time.Sleep(5 * time.Second)
}


//export go_func
func go_func(fpBug C.void_arg_fn) {
   ch := make(chan chanType, 10)

   go foo_go(ch,fpBug)
   bar_go(ch)
   time.Sleep(5 * time.Second)
}



func main(){}


