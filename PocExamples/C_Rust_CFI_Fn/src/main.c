#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


extern void foo_rust(void(*)(int),void(*)(void (**)(int)));


void foo_int(int a){
   printf("-This is foo_int(C)\n\n");
  
}

void foo_float(float a){
   printf("-This is foo_float(C)\n\n");
  
}


void bug (void (**fpC) (int)){

   *fpC = (void(*)(int))&foo_float;

}

int main(){
   
   foo_rust(&foo_int,&bug);
   return 0;
}


