#[no_mangle]
pub extern fn foo_rust(fp:fn(i32), bug:fn(&Fn(i32))) {
   foo_rust1(fp,bug);
}



pub extern fn foo_rust1<F>(fp:F, bug:fn(&Fn(i32))) where F:Fn(i32){
   let a:i32 = 0 ; 
   bug(&fp);
   println!("-This is foo_rust(Rust)\n");
   fp(a);
}

