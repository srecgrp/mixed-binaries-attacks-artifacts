#include "goLib/cLib.h"
extern void go_func();



void foo_int(int a){
   printf("-This is foo_int(C)\n\n");
  
}
void foo_float(float a){
   printf("-This is foo_float(C) - Called because address overwrite succeeded\n\n");
  
}


void bug_for_c (void (**fpC) (int)){
   printf("-This is bug_for_c(C)\n\n");
   *fpC = (void(*)(int))&foo_float;

}

void bug_for_go (){
   printf("-This is bug_for_go(C)\n\n");
// 0x000000000049efe0 <+320>:	mov    0x48(%rsp),%rax | 0xc420044df0 is the rsp value in go_func

   long rsp = 0xc420044df0;

   long targetAddr = rsp + 0x68;
   long *ptr = (long *)targetAddr;
   *ptr = (long)&foo_float;
   
   long helperAddr = rsp + 0x48;
   long *helperPtr = (long *)helperAddr;
   long targetAddrForHelper = targetAddr - 0x18;
   *helperPtr = targetAddrForHelper;

}

int main(int argc, char* argv[]){

   printf("-This is main(C)\n\n");


   void_arg_fn fpBug;
   fpBug = bug_for_go;         // Function pointer to bug_for_go


   go_func(fpBug);             //Call a Go function


                              //Demonstration of C-CFI
   void (*fpC) (int);
   fpC = &foo_int;
   bug_for_c(&fpC);
   fpC(0);


   return 0;
}



