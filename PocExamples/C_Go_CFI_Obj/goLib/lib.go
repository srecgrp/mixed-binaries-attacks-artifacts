package main

/*#cgo CFLAGS: -g -O0
#include "cLib.h" */ 
import "C" 
    
import (
   "fmt"
)
type Animal interface {
   make_sound(words string)
}

type Cat struct {}

type Dog struct {}

func (c *Cat) make_sound(words string) {
   fmt.Printf("-"+words + "\n\n")
}

func (d *Dog) make_sound(words string) {
   fmt.Printf("-"+words + "\n\n")
}



//export go_func
func go_func (fpBug C.void_arg_fn) {
   fmt.Println("-This is go_func(Go)\n")


   var v1, v2 Animal


   v1 = new(Cat)
   v2 = new(Dog)
   C.bridge(fpBug);           //"Bug"
   v1.make_sound("Meow")
   v2.make_sound("Woof")

}

func main(){}


