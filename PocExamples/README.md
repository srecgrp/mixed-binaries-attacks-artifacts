Build : "make"  

Run   : "./target/main 0" ( "./target/main 1" for the C++ part in  the following [d] and [e] cases )


a.	C_Rust_CFI 
This is a PoC example of a Rust function calling a C function through an indirect branch without using the "unsafe" keyword. Initially, the function "bug" in C overwrites the address of the indirect branch. Then, when Rust calls C with the "f" function pointer, it calls the wrong function and CFI does not  crash the program, despite being enabled.

b.	C_Rust_CFI_Fn
This is a PoC example of a Rust function calling a C function through an indirect branch without using the "unsafe" keyword. Initially, the function "bug" in C overwrites the address of the indirect branch in Rust. Then, when Rust calls C with the "fp" trait, it calls the wrong function and CFI does not crash the program, despite being enabled.

c.	C_Rust_CFI_Unsafe
This is a PoC example of a Rust function calling a C function through an indirect branch using the "unsafe" keyword. Initially, the function "bug" in C overwrites the address of the indirect branch in Rust. Then, when Rust calls C with the "f" function pointer, it calls the wrong function and CFI does not crash the program, despite being enabled.

d.	CPP_Rust_C_CFI
This is a PoC example of combining C, C++ and Rust. Firstly, C calls either Rust or C++, depending on the input(0 for Rust - 1 for C++). For the Rust part, the foo_rust() function calls the bug() function which overwrites the address that will be used later in the indirect branch  "animal.make_sound()". As a result, the program jumps to the wrong function without crashing, even though CFI is enabled. In contrast, for the C++ part, when foo_cpp() function calls the bug() function it overwrites the address that will be used later in the indirect branch  (*it)->make_sound().c_str(). But in this case the program crashes because the CFI can detect an illegal jump originated from C++.

e.	CPP_Rust_C_noCFI
This is a PoC example of combining C, C++ and Rust. Firstly, C calls either Rust or C++, depending on the input(0 for Rust - 1 for C++). In both cases, the program jumps to the wrong functions because CFI is disabled and there is no defence against an illegal jump. 

f.	SafeStack_C_Rust
This is a simplified PoC example for bypassing SafeStack. In this example, SafeStack separates the control data  from the buffers that could potentially overwrite them. However, since SafeStack does not work for Rust, a buffer that is created from Rust code, remains in the same stack as the control data. As a result, when then program is given the appropriate input (shown in the main.c file) the return address of the function "foo_rust" is overwritten by the "buffer" buffer.

g.	vTable_call_another_function
This is a PoC example of a Rust function calling a C function through an indirect branch using the "unsafe" keyword (bug(x);). Initially, the function "bug" in C overwrites the address of the next indirect branch in Rust. Then, when Rust calls "animal.make_sound()" , it calls the wrong function(foo_C) and CFI does not crash the program, despite being enabled.

h.	vTable_call_another_vTable
This is a PoC example of a Rust function calling a C function through an indirect branch using the "unsafe" keyword (bug();). Initially, the function "bug" in C overwrites the address of the next indirect branch in Rust. Then, when Rust calls "animal.make_sound()" , it calls a function from the other VTable (Dog's VTable function instead of Cat's VTable) and CFI does not crash the program, despite being enabled.

i.	C_Go_CFI 
This is a PoC example of a Go function calling a C function through an indirect branch using a function pointer. Initially, the function "bug_for_go" in C overwrites the address of the indirect branch. Then, when Go calls "bar_go_int" with the "fpGo" function pointer, it calls the wrong function and CFI does not crash the program, despite being enabled.

j.	C_Go_CFI_Channels 
This is a PoC example of a Go function calling a C function through an indirect branch using a channel. Initially, the function "bug_for_go" in C overwrites the address of the indirect branch. Then, when Go calls the function that the channel "e" holds, it calls the wrong function and CFI does not crash the program, despite being enabled.

k.	C_Go_CFI_Closures 
This is a PoC example of a Go function calling a C function through an indirect branch using a closure. Initially, the function "bug_for_go" in C overwrites the address of the indirect branch. Then, when Go calls the closure returned from "sum" function, it calls the wrong function and CFI does not crash the program, despite being enabled.

l.	C_Go_CFI_Obj 
This is a PoC example of a Go function calling a C function through an indirect branch using an object.  Initially, the function "bug_for_go" in C overwrites the address of the indirect branch. Then, when Go calls "v2.make_sound("Woof")", it calls the wrong function and CFI does not crash the program, despite being enabled.

