#[no_mangle]
pub extern fn foo_rust(){

	
	let dog: Dog = Dog;
	let cat: Cat = Cat;
	let mut v: Vec<Box<Animal>> = Vec::new();
	
	v.push(Box::new(cat));
	v.push(Box::new(dog));
	unsafe{
	   let x: usize = &v[1] as *const _ as usize;
   	bug(x);
	}
	for animal in v.iter() {


		println!("{}", animal.make_sound());
	}

}

extern {
    fn bug(addr: usize);
}


trait Animal {
    fn make_sound(&self) -> String;
}

struct Cat;
impl Animal for Cat {
    fn make_sound(&self) -> String {
        "meow".to_string()
    }
}

struct Dog;
impl Animal for Dog {
    fn make_sound(&self) -> String {
        "woof".to_string()
    }
}


