#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


extern void foo_rust(void);



void foo_C(){
   printf("-This is foo_C(C)\n\n");
  
}


void bug (long targetAddr){
	targetAddr += 0x8;
	printf("bug\n");

	long *ptr1 = (long *)targetAddr;
	long *ptr2 = (long *)(targetAddr+0x30);
	long fooAddress =(long)&foo_C;

	*ptr1 = (long) (targetAddr+0x18);
	*ptr2 =  fooAddress;
}


int main(int argc, char* argv[]){

   if(argc != 1){
      foo_rust();
   }  
   return 0;

}


