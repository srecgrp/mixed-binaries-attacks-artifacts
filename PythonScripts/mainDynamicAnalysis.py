import subprocess
import signal
import os
import sys 

def getTimes():
	fileName = "indirectFunctions.txt"
	fileObj = open(fileName, "r").readlines()
	return len(fileObj)/20

def printOut(outObj):
	
	for inCall in outObj:
		print(inCall)


def writeTimes(i):
	fileName = "funcTimes.txt"
	fileObj = open(fileName, "w")
	fileObj.write(str(i))
	fileObj.close()

def readOutput():
	output = "output.txt"
	outObj = open(output, "r").readlines()
	return outObj

def writeOutput(output):
	
	finalOutput = "finalOutput.txt"
	outObj = open(finalOutput, "a")
	outObj.write("\n")
	for inCall in output:
		outObj.write(str(inCall))
	outObj.write("\n")
	outObj.close()

	
def main(exPath):
	FNULL = open(os.devnull, 'w')
	times = getTimes()	
	for i in range(0,times):
		writeTimes(i)
		gdb = subprocess.call(["gdb","--command","dynamicAnalysis.py",exPath], stdout=FNULL, shell=False)
		output = readOutput()
		writeOutput(output)



if __name__:
	if len(sys.argv)== 2:
		exPath = sys.argv[1]
	else:
		print("\nPlease provide the executable's path.\n")
	main(exPath)

