a.	staticAnalysis.py
Run: "python staticAnalysis.py [system] [folder containing assembly files/assembly file path]"
This script takes a folder containing assembly files or an assembly file and creates a list of functions (indirectFunctions.txt) that contain at least one indirect branch. This list is examined by the dynamic analysis script in order to find the indirect branches that are executed during the program's run.
The assembly files of Mozilla Firefox 66.0a1 (2019-03-13) (64-bit) that were examined for for this paper are created by enabling RUSTFLAGS="--emit asm". 
In order to produce an assembly file for a project written in Go execute: "go tool objdump [input's name] > asm.s"


b.	dynamicAnalysis.py
Run: "gdb --command dynamicAnalysis.py [executable's Path]" 
Run with arguments for the executable: "gdb --command dynamicAnalysis.py --args [executable's Path] [ARGUMENTS]" 
The file created from the static analysis step should be in the same directory as dynamicAnalysis.py. In addition, a file named funcTimes.txt needs to be created with a "0" inside in order for this script to run (It is used for the communication between dynamicAnalysis.py and mainDynamicAnalysis.py).
This script takes an executable's path as an input and uses the list of functions created in the previous step in order to examine all the indirect branches located in those functions. 
Finally, it creates a list of indirect branches that contain information about the addresses they are executed, their target functions and whether they were used in the current execution of the program. The output of this script is "output.txt".
This script can run by itself for small executables. For analysing Firefox, we used mainDynamicAnalysis.py
to reduce the the number of breakpoints that were added for each execution and as a result reducing the overhead for each session.



c.	mainDynamicAnalysis.py
Run: "python mainDynamicAnalysis.py [executable's path]" 
The file created from the static analysis step should be in the same directory as mainDynamicAnalysis.py as well as the script dynamicAnalysis.py.
This script was created for automation reasons in order to examine all the functions' indirect branches created in the static analysis step. 
Because the dynamicAnalysis.py script, adds breakpoints to each one of the Rust functions that at have least one indirect branch, it is very slow to run Firefox with all the breakpoints at once. 
That is why we used this script that runs Firefox with 20 functions breakpoints each time by opening and closing Firefox after 5 minutes. The output of this script is "finalOutput.txt".


The scripts are written in Python.
