
import signal
import re
import os 
import subprocess
import threading
from subprocess import check_output

#Global Variable
indirectCalls = []
heapStr = 0
heapFin = 0
stackStr= 0
stackFin= 0
stop = False
minutes = 10
funcsSession = 20  #This number determines how many functions from "indirectFunctions.txt" 
                  #will be added as breakpoints per session.

#Prints a guide for running the dynamic analysis script.
def printHelp():
   print("Usage: gdb --command dynamicAnalysis.py [executable]")
   print("Example: gdb --command dynamicAnalysis.py test")



class DebugCalls(gdb.Breakpoint):
	
	def __init__(self,callAddress,callReg):
		self.counter = 0
		self.callAddress = callAddress
		self.callReg = self.cleanReg(callReg)
		super().__init__(callAddress)		

	def cleanReg(self,reg):
		reg = re.sub('[, ()]','',reg)     
		reg = re.sub('.*%','',reg)     
		return reg

	def getTargetFunction(self,symbolLog):
		for line in symbolLog:
			targetFunc = line
			targetFunc = re.sub(" in section.*\n","",targetFunc)
			targetFunc = re.sub(" ","",targetFunc)
			return targetFunc

	def getInfoSymbol(self):
		symbolCmd = "info symbol " + self.inAddress
		symbolLog = log("symbolInfo.txt",symbolCmd)
		self.targetFunction = self.getTargetFunction(symbolLog)

	def getIndirectAddress(self,regLog):
		for line in regLog:
			inAddress = line.split()[1]
			return inAddress	

	def getInfoReg(self):
		infoCmd = "info reg " + self.callReg
		regLog = log("callRegInfo.txt",infoCmd)
		self.inAddress = self.getIndirectAddress(regLog)


	def changeToStopped(self,callAddress):
		for inCall in indirectCalls:
			if(callAddress == inCall[1]):
				inCall[len(inCall)-1] = "Stopped"

	def changeTargetFunction(self,callAddress):
		for inCall in indirectCalls:
			if(callAddress == inCall[1]):
				inCall[len(inCall)-2] = self.targetFunction

	def changeTargetAddress(self,callAddress):
		for inCall in indirectCalls:
			if(callAddress == inCall[1]):
				inCall[len(inCall)-3] = self.inAddress

	def stop(self):
		if(self.counter <1):
			self.getInfoReg()
			self.getInfoSymbol()
			callAddress = re.sub("\*","",self.callAddress)
			self.changeToStopped(callAddress)
			self.changeTargetFunction(callAddress)
			self.changeTargetAddress(callAddress)
		self.counter +=1
		return stop


def log(fileName, command):
	osCmd = "rm "+fileName
	logFile = "set logging file "+fileName
	os.system(osCmd)
	gdb.execute(logFile)
	gdb.execute("set logging on")
	gdb.execute(command)
	gdb.execute("set logging off")
	gdbLog = open(fileName,"r").readlines()
	return gdbLog



class DebugMoves(gdb.Breakpoint):

	def __init__(self,function,callLine,movAddress,movReg,callReg):
		self.counter = 0
		self.function = function
		self.movAddress = movAddress
		self.movReg = movReg
		self.callLine = callLine
		self.callReg = callReg
		self.inAddress = 0
		super().__init__(movAddress)		

	def heapOrStack(self,inAddress):

		string = None
		memory = -1
		inAddress = int(inAddress,16)
		print("stackStr:" + str(stackStr) + "\tinAddress:" + str(inAddress) + "\tstackFin:" + str(stackFin)  +"\n")
		if(inAddress >= heapStr and inAddress <= heapFin):
			string = "-------------Heap-------------"			
			memory = 1
		elif(inAddress >= stackStr and stackFin <= stackFin):
			string = "-------------Stack-------------"
			memory = 0
		else:
			string = "-------------Not Found-------------"
			memory = -1
		return memory,string

	def getIndirectAddress(self,regLog):
		for line in regLog:
			inAddress = line.split()[1]
			return inAddress	


	def stop(self):
		global indirectCalls
		if(self.counter <1):
			infoCmd = "info reg " + self.movReg
			regLog = log("regInfo.txt",infoCmd)
			self.inAddress = self.getIndirectAddress(regLog)
			self.memoryPos,string = self.heapOrStack(self.inAddress)
			movAddress = re.sub("\*","",self.movAddress)
			addToIndirectCalls(self.function,self.callLine,self.callReg,movAddress,self.movReg,self.inAddress,self.memoryPos)
		self.counter += 1
		return stop


def addToIndirectCalls(function,callLine,callReg,movAddress,movReg,inAddress,memoryPos):
	tempIndirectCalls = []
	found = False
	for inCall in indirectCalls:
		if callLine == inCall[1]:
			found = True
	if not found:
		tempIndirectCalls.append(function)				# function
		tempIndirectCalls.append(callLine)				# call Address
		tempIndirectCalls.append(callReg)				# callRegister	
		tempIndirectCalls.append(movAddress)			# mov address
		tempIndirectCalls.append(movReg)					# mov Reg		
		tempIndirectCalls.append(inAddress)	 			# indirect address
		tempIndirectCalls.append(memoryPos)				# memoryPos	
		tempIndirectCalls.append("targetAddress")	   # targetAddress
		tempIndirectCalls.append("targetFunction")	# TargetFunction
		tempIndirectCalls.append("noStopped")			# Stopped

		indirectCalls.append(tempIndirectCalls)
		
	
class DebugFunctions(gdb.Breakpoint):

	def __init__(self,function):
		self.counter = 0
		self.function = function
		super().__init__(function)		

	def stop(self):
		print("------------")
		print(self.counter)


		print("------------")
		if(self.counter <1):
			self.putCallsBreakpoints()
		self.counter +=1
		return stop
   
	def getCallReg(self,instruction):
		callReg = re.sub('[*, ]','',instruction)  
		return callReg

	def getLeftPart(self,instruction):	
		leftPart = re.search(r'[a-zA-Z0-9%$(),-:]*,',instruction)
		if(leftPart):
			leftPart = leftPart.group()
			leftPart = re.sub('[, ()]','',leftPart)     
#			leftPart = re.sub('.*%','',leftPart)     
			return leftPart
		else:
			return None

	def cleanReg(self,reg):
		reg = re.sub('[, ()]','',reg)     
		reg = re.sub('.*%','',reg)     
		return reg

	def getRightPart(self,instruction):
		rightPart =  re.search(r',[a-zA-Z0-9%$(),-:]*$',instruction) 
		if(rightPart):
			rightPart = rightPart.group()
			rightPart = re.sub('[, ()]','',rightPart)   
			#rightPart = re.sub('.*%','',rightPart)      
			return rightPart
		else:
			return None

	def getFirstMove(self,disas,lastMov):
		movAddress = disas[lastMov][0]
		movReg = self.getLeftPart(disas[lastMov][1][1])
		leftReg = self.getLeftPart(disas[lastMov][1][1])
		for tempLine in range(lastMov-1,0,-1):

			rightPart =  self.getRightPart(disas[tempLine][1][1])
			if("mov" in disas[tempLine][1][0] and leftReg in rightPart):
				leftReg = self.getLeftPart(disas[tempLine][1][1])
				movAddress = disas[tempLine][0]
				movReg = self.getLeftPart(disas[tempLine][1][1])

		movReg = self.cleanReg(movReg)
		movAddress = "*" + movAddress
		return movAddress, movReg

	def getLastMove(self,disas,tempLine):
		movAddress = disas[tempLine][0]
		movReg = self.getLeftPart(disas[tempLine][1][1])
		movReg = self.cleanReg(movReg)
		movAddress = "*" + movAddress
		return movAddress,movReg

	def putMovesBreakPoints(self,disas,callLine):
		callReg = self.getCallReg(disas[callLine][1][1])
		movFound = False
		for tempLine in range(callLine,0,-1):
			rightPart =  self.getRightPart(disas[tempLine][1][1])
			if(("mov" in disas[tempLine][1][0] or "lea" in disas[tempLine][1][0])   and callReg[1:] in rightPart):
				movAddress, movReg = self.getLastMove(disas,tempLine) #self.getFirstMove(disas,tempLine) #
				DebugMoves(self.function,disas[callLine][0],movAddress,movReg,callReg)
				movFound = True
				break		
		return movFound

	def putCallsBreakpoints(self):
		gdbLog = log("gdbLogFile.txt","disassemble")[1:]
		gdbLog[len(gdbLog)-1:]
		disas = self.createDisas(gdbLog)
		for line in range (0,len(disas)):
			if ("callq" in disas[line][1] and "*" == disas[line][1][1][0]):
				movFound = self.putMovesBreakPoints(disas,line)
				
				address = disas[line][0]
				address = "*" + address
				
				callReg = self.getCallReg(disas[line][1][1])
				if not movFound :

					callLine = disas[line][0]
					movAddress = "Nan"
					movReg = "Nan"
					inAddress = "Nan"
					memoryPos = -2
					addToIndirectCalls(self.function,callLine,callReg,movAddress,movReg,inAddress,memoryPos)
				DebugCalls(address,callReg)   

					

	def getAddress(self,line):
		address = line.split()
		if "=>" in address[0]:
			return address[1]
		else:
			return address[0]

		


	def getInstruction(self,line):
		instructionList = []
		instruction = re.search(r":.*",line)
		if(instruction):	
			instruction = instruction.group()
			instruction = re.sub(r":\s*","",instruction)
			instruction = instruction.split()
			instructionList.append(instruction[0])
			if(len(instruction)>1):
				instructionList.append(instruction[1])
			else:
				instructionList.append("")
		return instructionList

	def createDisas(self,gdbLog):
		disas = []
		tempDisas = []
		for line in gdbLog:
			address = self.getAddress(line)
			instruction = self.getInstruction(line)
			tempDisas.append(address)
			tempDisas.append(instruction)
			disas.append(tempDisas)
			tempDisas = []	
		return disas


def getSpectrum(pid):
	global heapStr 
	global heapFin 
	global stackStr
	global stackFin
	global indirectCalls

	print (pid)
	fileName = "/proc/"+pid+"/maps"
	print(fileName)
	if(os.path.exists(fileName)):
		maps = open(fileName, "r")
		for line in maps:
			match = re.search(r'heap', line)
			if (match):
				print(line)
				heapSpec = line.split()
				heapSpec = heapSpec[0]
				heapSpec = heapSpec.split("-")
				heapStr = heapSpec[0]
				heapStr = int(heapStr,16)
				heapFin = heapSpec[1]
				heapFin = int(heapFin,16)
			match = re.search(r'stack', line)
			if (match):
				print(line)
				stackSpec = line.split()
				stackSpec = stackSpec[0]
				stackSpec = stackSpec.split("-")
				stackStr = stackSpec[0]
				stackStr = int(stackStr,16)
				stackFin = stackSpec[1]
				stackFin = int(stackFin,16)
		if(heapStr == 0 or heapFin == 0):
			print("\n-Heap wasn't found.\n")
	else:
		print("\n-Process ended.(Process not Found)\n")
		os._exit(0)
	return heapStr,heapFin,stackStr,stackFin

def getPid(name):
	pid = 0
	print(name)
	try:
		pid = check_output(["pgrep","-n",name])
	except subprocess.CalledProcessError as e:
		print("\n-Process ended.(getPid)\n")
		os._exit(0)
	pid = str(pid)
	pid = re.sub(r'[^0-9 ]','',pid)
	print(pid)
	pid = pid.split()[0]
	return pid

def getIndirectFunctions():
	fileName = "indirectFunctions.txt"
	if(os.path.isfile(fileName)):
		
		indirectFunctions = []
		fileObj = open(fileName,'r')
		for function in fileObj:
			indirectFunctions.append(re.sub("[\n]","",function))
		return indirectFunctions
	else:
		print("\n-Please run static.py first and move \"indirectFunctions.txt\" to this directory.")
		os._exit(0)


def gdbStop(pid):
	seconds = int(minutes * 60)
	stopExe = threading.Timer(seconds,stopProc,(pid,))
	stopExe.start()

def stopProc(pid):
	pidPath = "/proc/" + str(pid)
	if(os.path.exists(pidPath)):
		print("\n-Process stopped\n")
		os.kill(int(pid), signal.SIGTERM )		

def classifyInCalls(memoryPos):
	string = ""

	if memoryPos == -2:
		string = "MovNotFound"
	elif memoryPos == -1:
		string = "NotFound"
	elif memoryPos == 1:
		string = "HEAP"		
	elif memoryPos == 0:
		string = "STACK"		
	return string

def writeIndirectCalls(start,finish):	

	fileName = "output.txt"
	fileObj = open(fileName, "w")
	fileObj.write("Times : " +str(start)  + "-" +str(finish)+"\n")
	fileObj.write("Heap: " +str(hex(heapStr)) +" - " +str(hex(heapFin))+"\n")
	fileObj.write("Stack: " +str(hex(stackStr)) +" - " +str(hex(stackFin))+"\n")
	for inCall in indirectCalls:
		inCall.append(	classifyInCalls(inCall[6]))
		for info in inCall:
			fileObj.write(str(info)+" ")
		fileObj.write("\n")
	print(len(indirectCalls))
	for inCall in indirectCalls:
		inCall.append(	classifyInCalls(inCall[6]))
		for info in inCall:
			print(str(info)+" ")
		print("\n")
	print ("-The script exited successfully. The analysis is located in output.txt.\n")

	
def getFunctions(indirectFunctions):
	fileName = "funcTimes.txt"
	fileObj = open(fileName, "r").readline()
	
	times = int(fileObj)
	start = funcsSession * times
	finish = start + funcsSession
	if finish > len(indirectFunctions) :
		print("-The number for functions per session is bigger than the number of indirect functions in indirectFunctions.txt. Please change the variable \"funcSession\".")
		os._exit(0)
	return start, finish


def printUsedFunctions(indirectFunctions,start, finish):
	fileName = "usedFuncs.txt"	
	fileObj = open(fileName, "a")
	for i in range(start,finish):
		fileObj.write(str(indirectFunctions[i])+"\n")

def init():
	gdb.execute("set pagination off")
	indirectFunctions = getIndirectFunctions()   
	start, finish = getFunctions(indirectFunctions)
	printUsedFunctions(indirectFunctions,start, finish)	
	print ("Times : " +str(start)  + "-" +str(finish))
	return indirectFunctions, start, finish


def createFuncTimes():
   fileObj = open("funcTimes.txt", "w")
   fileObj.write("0")


def main(fileName):        

   global stop
   option = 0
   if option == 0 :
      indirectFunctions, start, finish = init()
	
      if (not("firefox" in fileName)):
         gdb.execute("b main")
         print (fileName)

      gdb.execute("start")
      exePid = getPid(fileName)
      gdbStop(exePid)												
      getSpectrum(exePid)

      for function in range (start,finish):
         DebugFunctions(indirectFunctions[function])   
      gdb.execute("continue")										
      writeIndirectCalls(start,finish)
      os._exit(0)														



if __name__:
   fileName = gdb.current_progspace().filename
   if ( not fileName ):
      printHelp()
      os._exit(0)
   fileName = os.path.basename(fileName) 
   if(not os.path.isfile("funcTimes.txt")):
      createFuncTimes()
   main(fileName)




