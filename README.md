The paper named  "Exploiting Mixed Binaries" is accompanied by the following artifacts.

1. Several PoC examples that combine C/C++ hardened code (with CFI and SafeStack) with Rust/Go
code, and artificial exploits. The Rust programs come in the form of archives that are automatically
built using the cargo tool and utilize function pointers, closures, and traits. The Go programs utilize
function pointers, closures, interfaces and channels.

		More information in PocExamples/README.md  

2. Scripts that perform the static and dynamic analysis, as outlined in Section 6, in any version of Mozilla
Firefox, as well as in any C/C++ software that is partially composed by Rust/Go. The scripts are
written in Python.
  
		More information in PythonScripts/README.md
  
3. The PoC exploits, as outlined in Section 7 and in Section 8.1, for Mozilla Firefox 66.0a1 (2019-03-13)
(64-bit) and go-stats, respectively.

		More information in PocExploits/README.md  
	
